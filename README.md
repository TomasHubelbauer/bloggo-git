# Git

https://github.com/MichaelMure/git-bug

## Force rebase

http://kernowsoul.com/blog/2012/06/20/4-ways-to-avoid-merge-commits-in-git/

## Git configuration in repository

https://stackoverflow.com/a/18330114/2715716

Needs to be updated manually.

## Multiple identities - set per project repository

```sh
git config --local user.name "Tomas Hubelbauer"
git config --local user.email "tomas@hubelbauer.net"
```

## NodeJS Git Servers

- [NodeJS Git Server by Stackdot](https://github.com/stackdot/NodeJS-Git-Server)
    - [Package on NPM](https://www.npmjs.com/package/git-server)

- [Node Git Server by Gabriel Csapo](https://github.com/gabrielcsapo/node-git-server)
    - [Package on NPM](https://www.npmjs.com/package/node-git-server)

## [On undoing, fixing, or removing commits in Git](https://sethrobertson.github.io/GitFixUm/fixup.html)

## Annex

https://git-annex.branchable.com/

## LFS

## Appraise

Distributed code reviews.

https://github.com/google/git-appraise

## Dit

Distributed issue tracking.

https://github.com/neithernut/git-dit

## Git Repository Viewer (Terminal)

https://github.com/rgburke/grv

This is so good.
